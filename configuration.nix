# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [
      ./machine.nix
      # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  nix.settings.experimental-features = ["nix-command" "flakes"];
  nix.extraOptions =
    ''
      bash-prompt = \n\[\033[1;32m\][\[\e]0;nix develop: \w\a\]nix develop:\w]\$\[\033[0m\]\040
    '';

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Pick only one of the below networking options.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Easiest to use and most distros use this by default.

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.desktopManager.lxqt.enable = true;
  services.xserver.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
    extraPackages = haskellPackages: [haskellPackages.optics];
  };

  environment.shellAliases =
    {cab = "cabal-fmt --inplace *.cabal && cabal";};
  environment.variables = {
    _JAVA_AWT_WM_NONREPARENTING="1";
    RIPGREP_CONFIG_PATH="/home/rednaz/.ripgreprc";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services.xserver.libinput.touchpad = {
    naturalScrolling = true;
    middleEmulation = false;
    accelSpeed = "0.5";
  };
  services.xserver.libinput.mouse.naturalScrolling = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.rednaz = {
    isNormalUser = true;
    extraGroups = ["wheel"]; # Enable `sudo` for the user.
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
    with pkgs; [
      networkmanagerapplet
      dmenu
      firefox
      git
      meld
      ((emacsPackagesFor emacs).emacsWithPackages
        (
          emacsPackages:
          (
            with emacsPackages;
            [
              melpaStablePackages.haskell-mode
            ]
          )
        )
      )
      ripgrep
      file
      pdftk
      unzip
      okular
      oathToolkit
      python3
    ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
}
