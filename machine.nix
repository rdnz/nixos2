{ config, lib, pkgs, ... }:

{
  imports =
    [
      (fetchTarball {
        name = "nixos-hardware-2024-02-06";
        url = "https://github.com/NixOS/nixos-hardware/archive/6e5cc385fc8cf5ca6495d70243074ccdea9f64c7.tar.gz";
        sha256 = "1ps4ff4a0wa65lb4dxk0l8wxlc071i9fgpv5bpy9hcwzkv6r6cid";
      } + "/lenovo/thinkpad/x1/9th-gen")
    ];

  networking.hostName = "nixos2"; # Define your hostname.

  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;
  services.blueman.enable = true;
  services.fwupd.enable = true;
  services.xserver.dpi = 192;

  nixpkgs.config.allowUnfreePredicate =
    unfreePackage:
    builtins.elem
      (lib.getName unfreePackage)
      ["vscode" "vscode-with-extensions" "zoom"];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
    (with pkgs; [
      zoom-us
      chromium
      neovim
      (vscode-with-extensions.override {
        # When the extension is already available in the default
        # extensions set.
        vscodeExtensions = with vscode-extensions; [
          justusadam.language-haskell
          haskell.haskell
          ms-python.python
          vscodevim.vim
        ];
        # # Concise version from the vscode market place when not available
        # # in the default set.
        # ++ vscode-utils.extensionsFromVscodeMarketplace [
        #   {
        #     name = "code-runner";
        #     publisher = "formulahendry";
        #     version = "0.6.33";
        #     sha256 = "166ia73vrcl5c9hm4q1a73qdn56m0jc7flfsk5p5q41na9f10lb0";
        #   }
        # ];
      })
    ]);

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [8000 8080];

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "23.11"; # Did you read the comment?
}
